﻿// <copyright file="PornRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PornStarMovie.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using System.Threading.Tasks;
    using PornStarMovie.Data;
    using PornStarMovie.Repository.Interfaces;

    /// <summary>
    /// The PornRepository class
    /// </summary>
    /// <typeparam name="TEntity"> T type</typeparam>
    /// <seealso cref="IRepository{TEntity}" />
    public abstract class PornRepository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        private readonly DbContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="PornRepository{TEntity}"/> class.
        /// </summary>
        /// <param name="context">The database.</param>
        protected PornRepository(DbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Gets context
        /// Gets </summary>
        public DbContext Context
        {
            get
            {
                return this.context;
            }
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="entity">TEntity</param>
        public void Delete(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentException("Your input is null man");
            }

            this.context.Set<TEntity>().Remove(entity);
            this.context.SaveChanges();
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id">int</param>
        public void Delete(int id)
        {
            TEntity entity = this.GetByID(id);
            if (entity == null)
            {
                throw new ArgumentException("Your input is null man");
            }

            this.Delete(entity);
        }

        /// <summary>
        /// Find
        /// </summary>
        /// <param name="predicate">Expression</param>
        /// <returns>The value we need</returns>
        public IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get All the elements
        /// </summary>
        /// <returns>elements in the entity</returns>
        public IQueryable<TEntity> GetAll()
        {
            return this.context.Set<TEntity>();
        }

        /// <summary>
        /// get the element through the id
        /// </summary>
        /// <param name="id">int </param>
        /// <returns>element</returns>
        public abstract TEntity GetByID(int id);

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="entity">TEntity</param>
        public void Insert(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentException("Your input is null man");
            }

            this.context.Set<TEntity>().Add(entity);
            this.context.SaveChanges();
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="entity">TEntity</param>
        public void Update(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentException("Your input is null man");
            }

            this.context.Set<TEntity>().Attach(entity);
            this.context.SaveChanges();
        }
    }
}
