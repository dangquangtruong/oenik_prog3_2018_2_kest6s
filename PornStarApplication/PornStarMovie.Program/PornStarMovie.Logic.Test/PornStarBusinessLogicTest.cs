﻿// <copyright file="PornStarBusinessLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PornStarMovie.Logic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using PornStarMovie.Data;
    using PornStarMovie.Logic.Logics;
    using PornStarMovie.Repository.Interfaces;
    using PornStarMovie.Repository.Repository;

    /// <summary>
    /// PornStarBusinessLogicTest
    /// </summary>
    public class PornStarBusinessLogicTest
    {
        private Mock<IRepository<PornStars>> mockPornStar;
        private List<PornStars> listPornStars;
        private PornStarBusinessLogic pornStarLogic;

        /// <summary>
        /// Test
        /// </summary>
        [Test]
        public void GetThisChingChongGetAllYourPornStars()
        {
            var querry = this.pornStarLogic.GetAll();
            Assert.That(querry.Count(), Is.EqualTo(this.listPornStars.Count()));
            this.mockPornStar.Verify();
        }

        /// <summary>
        /// Test
        /// </summary>
        [Test]
        public void ChingChongShouldKnowTheIDOfThePornStar()
        {
            var res = this.pornStarLogic.GetById(1);
            Assert.That(res.PornStarName, Is.EqualTo("Miura Wakana"));
        }

        /// <summary>
        /// Test
        /// </summary>
        [Test]
        public void ChingChongMustBeAbleToDeleteTheElementNotNullThrowing()
        {
            Assert.That(
                () => this.pornStarLogic.Delete(null), Throws.ArgumentException);
        }

        /// <summary>
        /// Test
        /// </summary>
        [Test]
        public void ChingChongMustBeAbleToDeleteThePornStars()
        {
            int numberOfElement = this.listPornStars.Count();
            this.pornStarLogic.DeleteByID(1);
            Assert.That(this.listPornStars.Count(), Is.EqualTo(numberOfElement - 1));
        }

        /// <summary>
        /// Test
        /// </summary>
        [Test]
        public void GetPornStarByTheFetish()
        {
            var querry = this.pornStarLogic.GetPornStarByTheFetish("soft porn");
            Assert.That(querry.ToList(), Is.EqualTo(this.listPornStars));
        }

        /// <summary>
        /// SetUpTheTestRepository
        /// </summary>
        [SetUp]
        public void SetUpTheTestRepository()
        {
            this.listPornStars = new List<PornStars>()
            {
                new PornStars()
                {
                    PornStarID = 1,
                    PornStarSalary = 10000,
                    PornStarName = "Miura Wakana",
                    PornStarEmail = "MiuraWakana@gmail.com",
                    PornStarGender = "female",
                    PornStarFestishType = "soft porn"
                },
                new PornStars()
                {
                    PornStarID = 2,
                    PornStarSalary = 14000,
                    PornStarName = "Laura Takizawa",
                    PornStarEmail = "LauraTakizawa@gmail.com",
                    PornStarGender = "female",
                    PornStarFestishType = "soft porn"
                }
            };
            this.mockPornStar = new Mock<IRepository<PornStars>>();
            this.pornStarLogic = new PornStarBusinessLogic(this.mockPornStar.Object);
            this.mockPornStar.Setup(x => x.GetAll()).Returns(this.listPornStars.AsQueryable());
            this.mockPornStar.Setup(x => x.GetByID(It.IsAny<int>())).Returns((int id) => this.listPornStars.Where(x => x.PornStarID == id).FirstOrDefault());
            this.mockPornStar.Setup(x => x.Delete(It.IsAny<PornStars>())).Callback((PornStars a) => this.listPornStars.Remove(a));
            this.mockPornStar.Setup(x => x.Update(It.IsAny<PornStars>())).Callback(
                (PornStars a) =>
                {
                    var i = this.listPornStars.FindIndex(x => x.PornStarID == a.PornStarID);
                    if (i != -1)
                    {
                        this.listPornStars[i] = a;
                    }
                    else
                    {
                        this.listPornStars.Add(a);
                    }
                });
            this.mockPornStar.Setup(x => x.Delete(It.IsAny<int>())).Callback((int i) => this.listPornStars.RemoveAt(i));
        }
    }
}
