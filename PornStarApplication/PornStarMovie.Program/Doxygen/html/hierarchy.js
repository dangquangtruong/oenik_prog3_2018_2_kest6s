var hierarchy =
[
    [ "PornStarMovie.Data.Class1", "class_porn_star_movie_1_1_data_1_1_class1.html", null ],
    [ "PornStarMovie.Data.Company", "class_porn_star_movie_1_1_data_1_1_company.html", null ],
    [ "PornStarMovie.Logic.Test.CompanyBusinessLogicTest", "class_porn_star_movie_1_1_logic_1_1_test_1_1_company_business_logic_test.html", null ],
    [ "PornStarMovie.Data.Conn_PornStars_Movies", "class_porn_star_movie_1_1_data_1_1_conn___porn_stars___movies.html", null ],
    [ "DbContext", null, [
      [ "PornStarMovie.Data.PornDatabseEntities", "class_porn_star_movie_1_1_data_1_1_porn_databse_entities.html", null ]
    ] ],
    [ "PornStarMovie.Logic.LogicInterfaces.ICompanyLogic", "interface_porn_star_movie_1_1_logic_1_1_logic_interfaces_1_1_i_company_logic.html", [
      [ "PornStarMovie.Logic.Logics.CompanyBusinessLogic", "class_porn_star_movie_1_1_logic_1_1_logics_1_1_company_business_logic.html", null ]
    ] ],
    [ "PornStarMovie.Logic.LogicInterfaces.IMovieLogic", "interface_porn_star_movie_1_1_logic_1_1_logic_interfaces_1_1_i_movie_logic.html", [
      [ "PornStarMovie.Logic.Logics.MovieBusinessLogic", "class_porn_star_movie_1_1_logic_1_1_logics_1_1_movie_business_logic.html", null ]
    ] ],
    [ "PornStarMovie.Logic.LogicInterfaces.IPornStarLogic", "interface_porn_star_movie_1_1_logic_1_1_logic_interfaces_1_1_i_porn_star_logic.html", [
      [ "PornStarMovie.Logic.PornStarBusinessLogic", "class_porn_star_movie_1_1_logic_1_1_porn_star_business_logic.html", null ]
    ] ],
    [ "PornStarMovie.Repository.Interfaces.IRepository< T >", "interface_porn_star_movie_1_1_repository_1_1_interfaces_1_1_i_repository.html", null ],
    [ "PornStarMovie.Repository.Interfaces.IRepository< PornStarMovie.Data.Company >", "interface_porn_star_movie_1_1_repository_1_1_interfaces_1_1_i_repository.html", null ],
    [ "PornStarMovie.Repository.Interfaces.IRepository< PornStarMovie.Data.Movies >", "interface_porn_star_movie_1_1_repository_1_1_interfaces_1_1_i_repository.html", null ],
    [ "PornStarMovie.Repository.Interfaces.IRepository< PornStarMovie.Data.PornStars >", "interface_porn_star_movie_1_1_repository_1_1_interfaces_1_1_i_repository.html", null ],
    [ "PornStarMovie.Repository.Interfaces.IRepository< TEntity >", "interface_porn_star_movie_1_1_repository_1_1_interfaces_1_1_i_repository.html", [
      [ "PornStarMovie.Repository.PornRepository< TEntity >", "class_porn_star_movie_1_1_repository_1_1_porn_repository.html", null ]
    ] ],
    [ "PornStarMovie.Program.Menu.Menu", "class_porn_star_movie_1_1_program_1_1_menu_1_1_menu.html", [
      [ "PornStarMovie.Program.Menu.EntityMenu", "class_porn_star_movie_1_1_program_1_1_menu_1_1_entity_menu.html", null ]
    ] ],
    [ "PornStarMovie.Logic.Test.MovieBusinessLogicTest", "class_porn_star_movie_1_1_logic_1_1_test_1_1_movie_business_logic_test.html", null ],
    [ "PornStarMovie.Data.Movies", "class_porn_star_movie_1_1_data_1_1_movies.html", null ],
    [ "PornStarMovie.Repository.PornRepository< Company >", "class_porn_star_movie_1_1_repository_1_1_porn_repository.html", [
      [ "PornStarMovie.Repository.Repository.CompanyRepository", "class_porn_star_movie_1_1_repository_1_1_repository_1_1_company_repository.html", null ]
    ] ],
    [ "PornStarMovie.Repository.PornRepository< Conn_PornStars_Movies >", "class_porn_star_movie_1_1_repository_1_1_porn_repository.html", [
      [ "PornStarMovie.Repository.Repository.ConnectorTableRepository", "class_porn_star_movie_1_1_repository_1_1_repository_1_1_connector_table_repository.html", null ]
    ] ],
    [ "PornStarMovie.Repository.PornRepository< Movies >", "class_porn_star_movie_1_1_repository_1_1_porn_repository.html", [
      [ "PornStarMovie.Repository.Repository.MovieRepository", "class_porn_star_movie_1_1_repository_1_1_repository_1_1_movie_repository.html", null ]
    ] ],
    [ "PornStarMovie.Repository.PornRepository< PornStars >", "class_porn_star_movie_1_1_repository_1_1_porn_repository.html", [
      [ "PornStarMovie.Repository.PornStarRepository", "class_porn_star_movie_1_1_repository_1_1_porn_star_repository.html", null ]
    ] ],
    [ "PornStarMovie.Logic.Test.PornStarBusinessLogicTest", "class_porn_star_movie_1_1_logic_1_1_test_1_1_porn_star_business_logic_test.html", null ],
    [ "PornStarMovie.Data.PornStars", "class_porn_star_movie_1_1_data_1_1_porn_stars.html", null ],
    [ "PornStarMovie.Program.Program", "class_porn_star_movie_1_1_program_1_1_program.html", null ]
];