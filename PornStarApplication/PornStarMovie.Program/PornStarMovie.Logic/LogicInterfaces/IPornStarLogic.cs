﻿// <copyright file="IPornStarLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PornStarMovie.Logic.LogicInterfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PornStarMovie.Data;
    using PornStarMovie.Repository;
    using PornStarMovie.Repository.Interfaces;

    /// <summary>
    /// IPornStarLogic
    /// </summary>
    public interface IPornStarLogic
    {
        /// <summary>
        /// GetPornStarByTheFetish
        /// </summary>
        /// <param name="fetish">string</param>
        /// <returns>PornStars</returns>
        IQueryable<PornStars> GetPornStarByTheFetish(string fetish);
    }
}
