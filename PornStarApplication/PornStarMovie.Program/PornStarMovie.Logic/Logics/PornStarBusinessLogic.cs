﻿// <copyright file="PornStarBusinessLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PornStarMovie.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PornStarMovie.Data;
    using PornStarMovie.Logic.LogicInterfaces;
    using PornStarMovie.Repository;
    using PornStarMovie.Repository.Interfaces;
    using PornStarMovie.Repository.Repository;

    /// <summary>
    /// PornStarBusinessLogic
    /// </summary>
    public class PornStarBusinessLogic : IPornStarLogic
    {
        private readonly IRepository<PornStars> pornRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="PornStarBusinessLogic"/> class.
        /// PornStarBusinessLogic
        /// </summary>
        /// <param name="pornRepository">PornStarRepository</param>
        public PornStarBusinessLogic(IRepository<PornStars> pornRepository)
        {
            this.pornRepository = pornRepository;
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="entity">PornStars</param>
        public void Delete(PornStars entity)
        {
            if (entity == null)
            {
                throw new ArgumentException("No pornstars for you Supreme Leader");
            }

            this.pornRepository.Delete(entity);
        }

        /// <summary>
        /// Delete the element by knowing its id
        /// </summary>
        /// <param name="id">int</param>
        public void DeleteByID(int id)
        {
            this.pornRepository.Delete(id);
        }

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="entity">PornStars</param>
        public void Insert(PornStars entity)
        {
            this.pornRepository.Insert(entity);
        }

        /// <summary>
        /// Modify
        /// </summary>
        /// <param name="entity">PornStars</param>
        public void Modify(PornStars entity)
        {
            this.pornRepository.Update(entity);
        }

        /// <summary>
        /// GetAll the element
        /// </summary>
        /// <returns>IQueryable object</returns>
        public IQueryable<PornStars> GetAll()
        {
            return this.pornRepository.GetAll();
        }

        /// <summary>
        /// Get the element by its id
        /// </summary>
        /// <param name="id">int type</param>
        /// <returns>PornStars entity</returns>
        public PornStars GetById(int id)
        {
            return this.pornRepository.GetByID(id);
        }

        /// <summary>
        /// GetPornStarByTheFetish
        /// </summary>
        /// <param name="fetish">string</param>
        /// <returns>PornStars</returns>
        public IQueryable<PornStars> GetPornStarByTheFetish(string fetish)
        {
            // return this.pornRepository.GetAll().Where(pornStar => pornStar.PornStarFestishType == fetish);
            if (this.pornRepository.GetAll().Where(pornStar => pornStar.PornStarFestishType == fetish) == null)
            {
                throw new ArgumentNullException("No fetish found");
            }

            if (this.pornRepository.GetAll().Count(pornStar => pornStar.PornStarFestishType == fetish) == 0)
            {
                throw new ArgumentNullException("No fetish found");
            }

            return this.pornRepository.GetAll().Where(pornStar => pornStar.PornStarFestishType == fetish);
        }
    }
}
