﻿// <copyright file="EntityMenu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PornStarMovie.Program.Menu
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using PornStarMovie.Data;
    using PornStarMovie.Logic;
    using PornStarMovie.Logic.Logics;
    using PornStarMovie.Repository;
    using PornStarMovie.Repository.Repository;

    /// <summary>
    /// Entity Menu
    /// </summary>
    public class EntityMenu : Menu
    {
        private readonly string[] entities;

        /// <summary>Initializes a new instance of the <see cref="EntityMenu"/> class.</summary>
        /// <param name="backString">The back string.</param>
        public EntityMenu(string backString = "back")
            : base(backString)
        {
            this.entities = new string[] { "pornstars", "companies", "movies", "java" };
        }

        /// <summary>Gets the selected entity.</summary>
        /// <value>The selected entity.</value>
        public string SelectedEntity
        {
            get;
            private set;
        }

= null;

        /// <summary>Writes out the menu items to be displayed.</summary>
        public override void OutputMenu()
        {
            for (int i = 0; i < this.entities.Length; i++)
            {
                System.Console.WriteLine(this.entities[i]);
            }

            System.Console.WriteLine("Back");
        }

        /// <summary>Processes the input.</summary>
        /// <param name="line">The line.</param>
        /// <returns>Returns true if input has been set.</returns>
        public override bool ProcessInput(string line)
        {
            if (this.entities.Contains(line.ToLower()))
            {
                this.SelectedEntity = line;
                switch (this.SelectedEntity)
                {
                    case "java":
                        {
                            WebClient wc = new WebClient();
                            string content = wc.DownloadString("http://localhost:8080/WebApplicationPorn/PornServlet");
                            Console.WriteLine(content);
                            break;
                        }

                    case "pornstars":
                        Console.WriteLine("selection: 1 - list, 2 - add, 3 - remove, 4 - modify, 5 - non crud, 6 - quit");
                        this.SelectionCRUD = int.Parse(Console.ReadLine());
                        switch (this.SelectionCRUD)
                        {
                            case 1:
                                Console.WriteLine();
                                foreach (var item in this.PornStarLogic.GetAll())
                                {
                                    Console.WriteLine($"{item.PornStarID} {item.PornStarName} {item.PornStarGender} {item.PornStarFestishType} {item.PornStarPhone} {item.PornstarDateOfBirth} {item.PornStarEmail} ");
                                }

                                break;
                            case 2:
                                {
                                    Console.WriteLine();
                                    Console.WriteLine("Put the Porn Star you want");
                                    PornStars pornStars = new PornStars() { };

                                    Console.WriteLine("the ID of the star: ");
                                    pornStars.PornStarID = int.Parse(Console.ReadLine());
                                    Console.WriteLine("the Name of the star: ");
                                    pornStars.PornStarName = Console.ReadLine();
                                    Console.WriteLine("the Salary of the star: ");
                                    pornStars.PornStarSalary = int.Parse(Console.ReadLine());
                                    Console.WriteLine("the Phone of the star: ");
                                    pornStars.PornStarPhone = Console.ReadLine();
                                    Console.WriteLine("the Email of the star: ");
                                    pornStars.PornStarEmail = Console.ReadLine();
                                    Console.WriteLine("the Date Of Birth of the star: ");
                                    pornStars.PornstarDateOfBirth = DateTime.Parse(Console.ReadLine());
                                    this.PornStarLogic.Insert(pornStars);
                                    break;
                                }

                            case 3:
                                {
                                    Console.WriteLine();
                                    Console.WriteLine("The ID you want to delete");
                                    int number = int.Parse(Console.ReadLine());
                                    this.PornStarLogic.DeleteByID(number);
                                    break;
                                }

                            case 4:
                                {
                                    Console.WriteLine();
                                    Console.WriteLine("Put the Porn Star you want");
                                    PornStars pornStars = new PornStars() { };

                                    Console.WriteLine("the ID of the star: ");
                                    pornStars.PornStarID = int.Parse(Console.ReadLine());
                                    Console.WriteLine("the Name of the star: ");
                                    pornStars.PornStarName = Console.ReadLine();
                                    Console.WriteLine("the Salary of the star: ");
                                    pornStars.PornStarSalary = int.Parse(Console.ReadLine());
                                    Console.WriteLine("the Phone of the star: ");
                                    pornStars.PornStarPhone = Console.ReadLine();
                                    Console.WriteLine("the Email of the star: ");
                                    pornStars.PornStarEmail = Console.ReadLine();
                                    Console.WriteLine("the fetish of the star: ");
                                    pornStars.PornStarFestishType = Console.ReadLine();
                                    Console.WriteLine("the Date Of Birth of the star: ");
                                    pornStars.PornstarDateOfBirth = DateTime.Parse(Console.ReadLine());
                                    this.PornStarLogic.Modify(pornStars);
                                    break;
                                }

                            case 5:
                                {
                                    Console.WriteLine();
                                    Console.WriteLine("Get Porn Stars By Their Fetish");
                                    Console.WriteLine("the fetish of the star: ");
                                    string pornStarFestishType = Console.ReadLine();
                                    try
                                    {
                                        foreach (var item in this.PornStarLogic.GetPornStarByTheFetish(pornStarFestishType))
                                        {
                                            Console.WriteLine($"{item.PornStarName} {item.PornstarDateOfBirth} {item.PornStarFestishType}");
                                        }
                                    }
                                    catch (ArgumentNullException)
                                    {
                                        Console.WriteLine("Not found, sorry");
                                    }

                                    break;
                                }

                            default:
                                {
                                    return false;
                                }
                        }

                        break;
                    case "movies":
                        Console.WriteLine("selection: 1 - list, 2 - add, 3 - remove, 4 - modify, 5 - non crud, 6 - quit");
                        this.SelectionCRUD = int.Parse(Console.ReadLine());
                        switch (this.SelectionCRUD)
                        {
                            case 1:
                                Console.WriteLine();
                                foreach (var item in this.MovieLogic.GetAll())
                                {
                                    Console.WriteLine($"{item.MovieID} {item.MovieName} {item.MovieDeadline} {item.MovieBudget} {item.MovieCompanyID}");
                                }

                                break;
                            case 2:
                                {
                                    Console.WriteLine();
                                    Console.WriteLine("Put the Movie you want");
                                    Movies movie = new Movies() { };

                                    Console.WriteLine("the ID of the movie: ");
                                    movie.MovieID = int.Parse(Console.ReadLine());
                                    Console.WriteLine("the Name of the movie: ");
                                    movie.MovieName = Console.ReadLine();
                                    Console.WriteLine("the Budget of the movie: ");
                                    movie.MovieBudget = int.Parse(Console.ReadLine());
                                    Console.WriteLine("the Company ID of the movie: ");
                                    movie.MovieCompanyID = int.Parse(Console.ReadLine());
                                    Console.WriteLine("the Date Of Deadline of the movie: ");
                                    movie.MovieDeadline = DateTime.Parse(Console.ReadLine());
                                    this.MovieLogic.Insert(movie);
                                    break;
                                }

                            case 3:
                                {
                                    Console.WriteLine();
                                    Console.WriteLine("The ID you want to delete");
                                    int number = int.Parse(Console.ReadLine());
                                    this.MovieLogic.DeleteByID(number);
                                    break;
                                }

                            case 4:
                                {
                                    Console.WriteLine();
                                    Console.WriteLine("Put the Movie you want");
                                    Movies movie = new Movies() { };

                                    Console.WriteLine("the ID of the movie: ");
                                    movie.MovieID = int.Parse(Console.ReadLine());
                                    Console.WriteLine("the Name of the movie: ");
                                    movie.MovieName = Console.ReadLine();
                                    Console.WriteLine("the Budget of the movie: ");
                                    movie.MovieBudget = int.Parse(Console.ReadLine());
                                    Console.WriteLine("the Company ID of the movie: ");
                                    movie.MovieCompanyID = int.Parse(Console.ReadLine());
                                    Console.WriteLine("the Date Of Deadline of the movie: ");
                                    movie.MovieDeadline = DateTime.Parse(Console.ReadLine());
                                    this.MovieLogic.Modify(movie);
                                    break;
                                }

                            case 5:
                                {
                                    Console.WriteLine();
                                    Console.WriteLine("Get the movies that has the maximum budget base on their company");
                                    try
                                    {
                                        foreach (var item in this.MovieLogic.MaxBudgeMovieCompany())
                                        {
                                            Console.WriteLine($"{item}");
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        Console.WriteLine("Not found, sorry");
                                    }

                                    break;
                                }

                            default:
                                {
                                    return false;
                                }
                        }

                        break;
                    case "companies":
                        Console.WriteLine("selection: 1 - list, 2 - add, 3 - remove, 4 - modify, 5 - non crud, 6 - quit");
                        this.SelectionCRUD = int.Parse(Console.ReadLine());
                        switch (this.SelectionCRUD)
                        {
                            case 1:
                                Console.WriteLine();
                                foreach (var item in this.ComLogic.GetAll())
                                {
                                    Console.WriteLine($"{item.CompanyID} {item.CompanyName} {item.ComapnyBudget} {item.CompanyWebsite} {item.CompanyCountry} ");
                                }

                                break;
                            case 2:
                                {
                                    Console.WriteLine();
                                    Console.WriteLine("Put the Company you want");
                                    Company company = new Company() { };

                                    Console.WriteLine("the ID of the company: ");
                                    company.CompanyID = int.Parse(Console.ReadLine());
                                    Console.WriteLine("the Name of the company: ");
                                    company.CompanyName = Console.ReadLine();
                                    Console.WriteLine("the Budget of the company: ");
                                    company.ComapnyBudget = int.Parse(Console.ReadLine());
                                    Console.WriteLine("the website of the company: ");
                                    company.CompanyWebsite = Console.ReadLine();
                                    Console.WriteLine("the Country of the company: ");
                                    company.CompanyCountry = Console.ReadLine();
                                    this.ComLogic.Insert(company);
                                    break;
                                }

                            case 3:
                                {
                                    Console.WriteLine();
                                    Console.WriteLine("The ID you want to delete");
                                    int number = int.Parse(Console.ReadLine());
                                    this.ComLogic.DeleteByID(number);
                                    break;
                                }

                            case 4:
                                {
                                    Console.WriteLine();
                                    Console.WriteLine("Put the Company you want");
                                    Company company = new Company() { };

                                    Console.WriteLine("the ID of the company: ");
                                    company.CompanyID = int.Parse(Console.ReadLine());
                                    Console.WriteLine("the Name of the company: ");
                                    company.CompanyName = Console.ReadLine();
                                    Console.WriteLine("the Budget of the company: ");
                                    company.ComapnyBudget = int.Parse(Console.ReadLine());
                                    Console.WriteLine("the website of the company: ");
                                    company.CompanyWebsite = Console.ReadLine();
                                    Console.WriteLine("the Country of the company: ");
                                    company.CompanyCountry = Console.ReadLine();
                                    this.ComLogic.Modify(company);
                                    break;
                                }

                            case 5:
                                {
                                    Console.WriteLine();
                                    Console.WriteLine("Get Movies By Their Company");
                                    Console.WriteLine("the company's name: ");
                                    string companyName = Console.ReadLine();
                                    try
                                    {
                                        foreach (var item in this.ComLogic.GetMoviesByCompany(companyName))
                                        {
                                            Console.WriteLine($"{item.MovieName} {item.MovieBudget} {companyName}");
                                        }
                                    }
                                    catch (ArgumentException)
                                    {
                                        Console.WriteLine("Not found, sorry");
                                    }

                                    break;
                                }

                            default:
                                {
                                    return false;
                                }
                        }

                        break;
                }

                return false;
            }
            else if (line == "shit")
            {
                Console.WriteLine();
                Console.WriteLine("The ship has been sinked, pls get out, press any key to exit");
                Console.ReadKey();
                return true;
            }
            else
            {
                this.SetErrorMessage("No such entity.");
                return false;
            }
        }
    }
}
