﻿// <copyright file="MovieBusinessLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PornStarMovie.Logic.Logics
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using System.Threading.Tasks;
    using PornStarMovie.Data;
    using PornStarMovie.Logic.LogicInterfaces;
    using PornStarMovie.Repository.Interfaces;
    using PornStarMovie.Repository.Repository;

    /// <summary>
    /// MovieBusinessLogic class that using MovieRepository
    /// </summary>
    public class MovieBusinessLogic : IMovieLogic
    {
        private readonly IRepository<Movies> movieRepository;
        private readonly IRepository<Company> companyRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="MovieBusinessLogic"/> class.
        /// Movie Business Logic class
        /// </summary>
        /// <param name="movieRepository">movie</param>
        /// <param name="companyRepository">Company</param>
        public MovieBusinessLogic(IRepository<Movies> movieRepository, IRepository<Company> companyRepository)
        {
            this.movieRepository = movieRepository;
            this.companyRepository = companyRepository;
        }

        /// <summary>
        /// Delete the data
        /// </summary>
        /// <param name="entity">Movies entity</param>
        public void Delete(Movies entity)
        {
            if (entity == null)
            {
                throw new ArgumentException("Its null comrade general");
            }

            this.movieRepository.Delete(entity);
        }

        /// <summary>
        /// Delete the element by knowing its id
        /// </summary>
        /// <param name="id">int</param>
        public void DeleteByID(int id)
        {
            this.movieRepository.Delete(id);
        }

        /// <summary>
        /// Insert the data
        /// </summary>
        /// <param name="entity">Movies entity</param>
        public void Insert(Movies entity)
        {
            this.movieRepository.Insert(entity);
        }

        /// <summary>
        /// the Modify - Update method
        /// </summary>
        /// <param name="entity">Movies entity</param>
        public void Modify(Movies entity)
        {
            this.movieRepository.Update(entity);
        }

        /// <summary>
        /// GetAll the element
        /// </summary>
        /// <returns>IQueryable object</returns>
        public IQueryable<Movies> GetAll()
        {
            return this.movieRepository.GetAll();
        }

        /// <summary>
        /// Get the element by its id
        /// </summary>
        /// <param name="id">int type</param>
        /// <returns>Movies entity</returns>
        public Movies GetById(int id)
        {
            return this.movieRepository.GetByID(id);
        }

        /// <summary>
        /// Max Budget Movie Company
        /// </summary>
        /// <returns>Movies</returns>
        public List<string> MaxBudgeMovieCompany()
        {
            var companyListVar = from companyList in this.companyRepository.GetAll()
                                  select new
                                  {
                                      companyId = companyList.CompanyID,
                                      companyName = companyList.CompanyName
                                  };
            var maxBudgetquerry = from budget in this.GetAll()
                                  group budget by budget.MovieCompanyID into keyBudget
                                  select new
                                  {
                                      KeyCompanyID = (int)keyBudget.Key,
                                      MaxBudget = (int)keyBudget.Max(x => x.MovieBudget),
                                  };
            var joinList = from budget in maxBudgetquerry
                           join companygroup in companyListVar on budget.KeyCompanyID equals companygroup.companyId
                           select new
                           {
                               budget.KeyCompanyID,
                               budget.MaxBudget,
                               companygroup.companyName,
                           };

            List<string> maxBudgeMovieCompanyList = new List<string>();
            foreach (var item in joinList.ToList())
            {
                maxBudgeMovieCompanyList.Add($" Company's ID: {item.KeyCompanyID} | Copmany's Name {item.companyName} | Company Maximum Budget: {item.MaxBudget} | ");
            }

            return maxBudgeMovieCompanyList;
        }
    }
}
