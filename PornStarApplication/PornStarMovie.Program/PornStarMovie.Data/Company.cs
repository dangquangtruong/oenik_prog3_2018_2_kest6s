//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PornStarMovie.Data
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Company entity
    /// </summary>
    public partial class Company
    {
        /// <summary>
        /// Company constructor
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]

        public Company()
        {
            this.Movies = new HashSet<Movies>();
        }
        /// <summary>
        /// CompanyId INSTANCE
        /// </summary>
        public int CompanyID { get; set; }
        /// <summary>
        /// CompanyName instance
        /// </summary>
        public string CompanyName { get; set; }
        /// <summary>
        /// CompanyCountry instance
        /// </summary>
        public string CompanyCountry { get; set; }
        /// <summary>
        /// CompanyWebsite instance
        /// </summary>
        public string CompanyWebsite { get; set; }
        /// <summary>
        /// ComapnyBudget instance
        /// </summary>
        public Nullable<int> ComapnyBudget { get; set; }
        /// <summary>
        /// Movies instance
        /// </summary>

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Movies> Movies { get; set; }
    }
}
