﻿// <copyright file="PornStarRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PornStarMovie.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PornStarMovie.Data;
    using PornStarMovie.Repository.Interfaces;

    /// <summary>
    /// PornStarRepository
    /// </summary>
    public class PornStarRepository : PornRepository<PornStars>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PornStarRepository"/> class.
        /// PornStarRepository
        /// </summary>
        /// <param name="context">DbContext</param>
        public PornStarRepository(DbContext context)
            : base(context)
        {
        }

        /// <summary>
        /// GetByID
        /// </summary>
        /// <param name="id">int</param>
        /// <returns>PornStars</returns>
        public override PornStars GetByID(int id)
        {
            return this.GetAll().SingleOrDefault(yourDream => yourDream.PornStarID == id);
        }

        /// <summary>
        /// Modify
        /// </summary>
        /// <param name="pornStarID">int</param>
        /// <param name="pornStarName">string</param>
        /// <param name="pornstarDateOfBirth">DateTime</param>
        /// <param name="pornStarFestishType">string fetish</param>
        /// <param name="pornStarEmail">string email</param>
        /// <param name="pornStarPhone">pornStarPhone</param>
        /// <param name="pornStarGender">pornStarGender</param>
        public void Modify(int pornStarID, string pornStarName, DateTime pornstarDateOfBirth, string pornStarFestishType, string pornStarEmail, string pornStarPhone, string pornStarGender)
        {
            PornStars yourDream = this.GetByID(pornStarID);
            if (yourDream == null)
            {
                throw new ArgumentException("Your dream porn star was not real man");
            }

            if (pornStarName != null)
            {
                yourDream.PornStarName = pornStarName;
            }

            if (pornstarDateOfBirth != null)
            {
                yourDream.PornstarDateOfBirth = pornstarDateOfBirth;
            }

            if (pornStarFestishType != null)
            {
                yourDream.PornStarFestishType = pornStarFestishType;
            }

            if (pornStarEmail != null)
            {
                yourDream.PornStarEmail = pornStarEmail;
            }

            if (pornStarPhone != null)
            {
                yourDream.PornStarPhone = pornStarPhone;
            }

            if (pornStarGender != null)
            {
                yourDream.PornStarGender = pornStarGender;
            }
        }
    }
}
