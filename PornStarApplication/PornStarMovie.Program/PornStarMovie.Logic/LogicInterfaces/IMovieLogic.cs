﻿// <copyright file="IMovieLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PornStarMovie.Logic.LogicInterfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PornStarMovie.Data;
    using PornStarMovie.Logic.Logics;
    using PornStarMovie.Repository;
    using PornStarMovie.Repository.Interfaces;

    /// <summary>
    /// IMovieLogic interface
    /// </summary>
    public interface IMovieLogic
    {
        /// <summary>
        /// Get Max Budge Movie Company
        /// </summary>
        /// <returns>Movies GroupBy</returns>
        List<string> MaxBudgeMovieCompany();
    }
}
