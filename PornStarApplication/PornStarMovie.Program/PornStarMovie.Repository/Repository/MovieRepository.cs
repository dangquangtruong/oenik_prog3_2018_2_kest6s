﻿// <copyright file="MovieRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PornStarMovie.Repository.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PornStarMovie.Data;
    using PornStarMovie.Repository.Interfaces;

    /// <summary>
    /// MovieRepository
    /// </summary>
    public class MovieRepository : PornRepository<Movies>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MovieRepository"/> class.
        /// MovieRepository
        /// </summary>
        /// <param name="context">DbContext</param>
        public MovieRepository(DbContext context)
            : base(context)
        {
        }

        /// <summary>
        /// GetByID
        /// </summary>
        /// <param name="id">int</param>
        /// <returns>movie.MovieID's value</returns>
        public override Movies GetByID(int id)
        {
            return this.GetAll().SingleOrDefault(movie => movie.MovieID == id);
        }

        /// <summary>
        /// Modify
        /// </summary>
        /// <param name="movieID">int</param>
        /// <param name="movieName">string</param>
        /// <param name="movieDeadline">DateTime</param>
        /// <param name="company">Company</param>
        public void Modify(int movieID, string movieName, DateTime movieDeadline, Company company)
        {
            Movies movie = this.GetByID(movieID);
            if (movie == null)
            {
                throw new ArgumentException("Your movie is not exist");
            }

            if (movieName != null)
            {
                movie.MovieName = movieName;
            }

            if (movieDeadline != null)
            {
                movie.MovieDeadline = movieDeadline;
            }

            if (company != null)
            {
                movie.MovieCompanyID = company.CompanyID;
                movie.Company = company;
            }
        }
    }
}
