﻿// <copyright file="ICompanyLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PornStarMovie.Logic.LogicInterfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PornStarMovie.Data;
    using PornStarMovie.Repository;
    using PornStarMovie.Repository.Interfaces;

    /// <summary>
    /// ICompanyLogic
    /// </summary>
    public interface ICompanyLogic
    {
        /// <summary>
        /// GetMoviesByCompany
        /// </summary>
        /// <param name="company">string</param>
        /// <returns>Movies</returns>
        IQueryable<Movies> GetMoviesByCompany(string company);
    }
}
