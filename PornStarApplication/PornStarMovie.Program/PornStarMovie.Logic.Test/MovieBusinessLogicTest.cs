﻿// <copyright file="MovieBusinessLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PornStarMovie.Logic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using PornStarMovie.Data;
    using PornStarMovie.Logic.LogicInterfaces;
    using PornStarMovie.Logic.Logics;
    using PornStarMovie.Repository.Interfaces;
    using PornStarMovie.Repository.Repository;

    /// <summary>
    /// MovieBusinessLogicTest
    /// </summary>
    public class MovieBusinessLogicTest
    {
        private Mock<IRepository<Movies>> mockMovies;
        private List<Movies> listMovies;
        private MovieBusinessLogic movieLogic;

        private Mock<IRepository<Company>> mockCompany;
        private List<Company> listCompany;

        /// <summary>
        /// Test
        /// </summary>
        [Test]
        public void GetThisChingChongGetAllYourMovies()
        {
            var querry = this.movieLogic.GetAll();
            Assert.That(querry.Count(), Is.EqualTo(this.listMovies.Count()));
            this.mockMovies.Verify();
        }

        /// <summary>
        /// Test
        /// </summary>
        [Test]
        public void ChingChongShouldKnowTheIDOfThePornStar()
        {
            var res = this.movieLogic.GetById(1);
            Assert.That(res.MovieName, Is.EqualTo("Say Hello to Her Little Friend Amina Danger"));
        }

        /// <summary>
        /// Test
        /// </summary>
        [Test]
        public void ChingChongMustBeAbleToDeleteTheElementNotNullThrowing()
        {
            Assert.That(
                () => this.movieLogic.Delete(null), Throws.ArgumentException);
        }

        /// <summary>
        /// Test
        /// </summary>
        [Test]
        public void ChingChongMustBeAbleToDeleteTheMovies()
        {
            int numberOfElement = this.listMovies.Count();
            this.movieLogic.DeleteByID(1);
            Assert.That(this.listMovies.Count(), Is.EqualTo(numberOfElement - 1));
        }

        /// <summary>
        /// Get Max Budget Movie Company
        /// </summary>
        [Test]
        public void GetMaxBudgeMovieCompanyTest()
        {
            var res = this.movieLogic.MaxBudgeMovieCompany();
            Assert.That(res.Count, Is.EqualTo(1));
        }

        /// <summary>
        /// Set Up The Test Repository
        /// </summary>
        [SetUp]
        public void SetUpTheTestRepository()
        {
            this.mockMovies = new Mock<IRepository<Movies>>();
            this.listMovies = new List<Movies>()
            {
                new Movies()
                {
                    MovieCompanyID = 1,
                    MovieID = 1,
                    MovieBudget = 10000,
                    MovieName = "Say Hello to Her Little Friend Amina Danger",
                    MovieDeadline = new DateTime(2018, 04, 30),
                },
                new Movies()
                {
                    MovieCompanyID = 1,
                    MovieID = 2,
                    MovieBudget = 20000,
                    MovieName = "Grateful For Cock Sarah Banks",
                    MovieDeadline = new DateTime(2018, 07, 07),
                }
            };
            this.mockMovies.Setup(x => x.GetAll()).Returns(this.listMovies.AsQueryable());
            this.mockMovies.Setup(x => x.GetByID(It.IsAny<int>()))
                .Returns((int id) => this.listMovies.Where(x => x.MovieID == id).FirstOrDefault());
            this.mockMovies.Setup(x => x.Delete(It.IsAny<Movies>())).Callback((Movies a) => this.listMovies.Remove(a));
            this.mockMovies.Setup(x => x.Update(It.IsAny<Movies>())).Callback(
                (Movies a) =>
                {
                    var i = this.listMovies.FindIndex(x => x.MovieID == a.MovieID);
                    if (i != -1)
                    {
                        this.listMovies[i] = a;
                    }
                    else
                    {
                        this.listMovies.Add(a);
                    }
                });
            this.mockMovies.Setup(x => x.Delete(It.IsAny<int>())).Callback((int i) => this.listMovies.RemoveAt(i));

            this.listCompany = new List<Company>()
            {
                new Company()
                {
                    CompanyID = 1,
                    ComapnyBudget = 10000,
                    CompanyCountry = "USA",
                    CompanyName = "Brazzer",
                    CompanyWebsite = "Something.com",
                },
                new Company()
                {
                    CompanyID = 2,
                    ComapnyBudget = 20000,
                    CompanyCountry = "Japan",
                    CompanyName = "BrazzerJAV",
                    CompanyWebsite = "SomethingSOME.com",
                }
            };
            this.mockCompany = new Mock<IRepository<Company>>();
            this.mockCompany.Setup(x => x.GetAll()).Returns(this.listCompany.AsQueryable());

            this.movieLogic = new MovieBusinessLogic(this.mockMovies.Object, this.mockCompany.Object);
        }
    }
}
