﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PornStarMovie.Repository.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using System.Threading.Tasks;
    using PornStarMovie.Data;

    /// <summary>
    /// The IRepository interface
    /// </summary>
    /// <typeparam name="T">T type</typeparam>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// Add a new T object to the database
        /// </summary>
        /// <param name="entity">The new object</param>
        void Insert(T entity);

        /// <summary>
        /// Remove the T object from the database
        /// </summary>
        /// <param name="entity">The T object what we want to delete</param>
        void Delete(T entity);

        /// <summary>
        /// Remove the T object from the database
        /// </summary>
        /// <param name="id">the id of the object we want to delete</param>
        void Delete(int id);

        /// <summary>
        /// Get all of the T object from the database
        /// </summary>
        /// <returns> The list of the T items </returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Find T elements according lambda expression
        /// </summary>
        /// <param name="predicate">The lambda expression for filtering</param>
        /// <returns>Return list of T item</returns>
        IQueryable<T> Find(Expression<Func<T, bool>> predicate);

        /// <summary>
        /// Return the object if exists in the database using id.
        /// </summary>
        /// <param name="id">the id of the object</param>
        /// <returns>the T object</returns>
        T GetByID(int id);

        /// <summary>
        /// Update method
        /// </summary>
        /// <param name="entity">T object</param>
        void Update(T entity);
    }
}
