﻿// <copyright file="CompanyBusinessLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PornStarMovie.Logic.Logics
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PornStarMovie.Data;
    using PornStarMovie.Logic.LogicInterfaces;
    using PornStarMovie.Repository.Interfaces;
    using PornStarMovie.Repository.Repository;

    /// <summary>
    /// CompanyBusinessLogic
    /// </summary>
    public class CompanyBusinessLogic : ICompanyLogic
    {
        private readonly IRepository<Company> companyRepository;
        private readonly IRepository<Movies> movieRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="CompanyBusinessLogic"/> class.
        /// Constructor
        /// </summary>
        /// <param name="companyRepository">CompanyRepository</param>
        /// <param name="movieRepository">MovieRepository</param>
        public CompanyBusinessLogic(IRepository<Company> companyRepository, IRepository<Movies> movieRepository)
        {
            this.companyRepository = companyRepository;
            this.movieRepository = movieRepository;
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="entity">Company</param>
        public void Delete(Company entity)
        {
            if (entity == null)
            {
                throw new ArgumentException("Its fucking null bitches");
            }

            this.companyRepository.Delete(entity);
        }

        /// <summary>
        /// Delete the element by knowing its id
        /// </summary>
        /// <param name="id">int</param>
        public void DeleteByID(int id)
        {
            if (this.companyRepository.GetAll().SingleOrDefault(company => company.CompanyID == id) == null)
            {
                throw new ArgumentException("Its fucking null bitches");
            }

            this.companyRepository.Delete(id);
        }

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="entity">Company</param>
        public void Insert(Company entity)
        {
            this.companyRepository.Insert(entity);
        }

        /// <summary>
        /// Modify
        /// </summary>
        /// <param name="entity">Company</param>
        public void Modify(Company entity)
        {
            this.companyRepository.Update(entity);
        }

        /// <summary>
        /// GetAll the element
        /// </summary>
        /// <returns>IQueryable object</returns>
        public IQueryable<Company> GetAll()
        {
            return this.companyRepository.GetAll();
        }

        /// <summary>
        /// Get the element by its id
        /// </summary>
        /// <param name="id">int type</param>
        /// <returns>Company entity</returns>
        public Company GetById(int id)
        {
            if (this.companyRepository.GetAll().SingleOrDefault(company => company.CompanyID == id) == null)
            {
                throw new ArgumentException("Its not valid, you will be sent to Gulag");
            }

            return this.companyRepository.GetByID(id);
        }

        /// <summary>
        /// GetMoviesByCompany
        /// </summary>
        /// <param name="companyName">string</param>
        /// <returns>Movies</returns>
        public IQueryable<Movies> GetMoviesByCompany(string companyName)
        {
            Company company = this.GetAll().SingleOrDefault(company1 => company1.CompanyName == companyName);
            if (company == null)
            {
                throw new ArgumentException("Not found the company of the Kaiser");
            }

            if (this.movieRepository.GetAll().Where(movie => movie.MovieCompanyID == company.CompanyID) == null)
            {
                throw new ArgumentException("the Company does not produce any movies");
            }

            if (this.movieRepository.GetAll().Count(movie => movie.MovieCompanyID == company.CompanyID) == 0)
            {
                throw new ArgumentException("the Company does not produce any movies");
            }

            return this.movieRepository.GetAll().Where(movie => movie.MovieCompanyID == company.CompanyID);
        }
    }
}
