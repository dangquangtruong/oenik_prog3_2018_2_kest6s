﻿// <copyright file="ConnectorTableRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PornStarMovie.Repository.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PornStarMovie.Data;
    using PornStarMovie.Repository.Interfaces;

    /// <summary>
    /// ConnectorTableRepository
    /// </summary>
    public class ConnectorTableRepository : PornRepository<Conn_PornStars_Movies>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectorTableRepository"/> class.
        /// ConnectorTableRepository
        /// </summary>
        /// <param name="context">DbContext</param>
        public ConnectorTableRepository(DbContext context)
            : base(context)
        {
        }

        /// <summary>
        /// Get the ID of the entity
        /// </summary>
        /// <param name="id">int type</param>
        /// <returns>conector id's value</returns>
        public override Conn_PornStars_Movies GetByID(int id)
        {
            return this.GetAll().SingleOrDefault(conn => conn.PornID == id);
        }
    }
}
