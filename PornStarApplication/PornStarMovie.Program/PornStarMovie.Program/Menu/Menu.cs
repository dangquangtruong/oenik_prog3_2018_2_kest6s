﻿// <copyright file="Menu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PornStarMovie.Program.Menu
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PornStarMovie.Data;
    using PornStarMovie.Logic;
    using PornStarMovie.Logic.Logics;
    using PornStarMovie.Repository;
    using PornStarMovie.Repository.Repository;

    /// <summary>
    /// The Menu definitions.
    /// </summary>
    public abstract class Menu
    {
        private readonly string backString;
        private string errorMessage = null;
        private int selectionCRUD;

        private PornDatabseEntities porn;

        private PornStarRepository pornRepository;
        private CompanyRepository companyRepo;
        private MovieRepository movieRepo;
        private PornStarBusinessLogic pornStarLogic;
        private MovieBusinessLogic movieLogic;
        private CompanyBusinessLogic comLogic;

        /// <summary>Initializes a new instance of the <see cref="Menu"/> class.</summary>
        /// <param name="backString">The back string.</param>
        public Menu(string backString = "back")
        {
            this.porn = new PornDatabseEntities();
            this.pornRepository = new PornStarRepository(this.porn);
            this.companyRepo = new CompanyRepository(this.porn);
            this.movieRepo = new MovieRepository(this.porn);
            this.PornStarLogic = new PornStarBusinessLogic(this.pornRepository);
            this.MovieLogic = new MovieBusinessLogic(this.movieRepo, this.companyRepo);
            this.ComLogic = new CompanyBusinessLogic(this.companyRepo, this.movieRepo);
            this.backString = backString;
        }

        /// <summary>
        /// Gets or sets selection: 1 - list, 2 - add, 3 - remove, 4 -modify.
        /// </summary>
        /// <value>
        /// The selection of crud operation .
        /// </value>
        public int SelectionCRUD { get => this.selectionCRUD; set => this.selectionCRUD = value; }

        /// <summary>
        /// Gets or sets company Logic
        /// </summary>
        public CompanyBusinessLogic ComLogic { get => this.comLogic; set => this.comLogic = value; }

        /// <summary>
        /// Gets or sets movie Logic
        /// </summary>
        public MovieBusinessLogic MovieLogic { get => this.movieLogic; set => this.movieLogic = value; }

        /// <summary>
        /// Gets or sets
        /// </summary>
        public PornStarBusinessLogic PornStarLogic { get => this.pornStarLogic; set => this.pornStarLogic = value; }

        /// <summary>
        /// Writes out the menu items to be displayed.
        /// </summary>
        public abstract void OutputMenu();

        /// <summary>Processes the input.</summary>
        /// <param name="line">The line.</param>
        /// <returns>Returns true if input has been set.</returns>
        public abstract bool ProcessInput(string line);

        /// <summary>
        /// Sets the error message that should be displayed based on the input.
        /// </summary>
        /// <param name="errorMessage">The error message that has to be displayed.</param>
        public void SetErrorMessage(string errorMessage)
        {
            this.errorMessage = errorMessage;
        }

        /// <summary>
        /// Displayes the error message and calls the <see cref="OutputMenu"/> to display the menu.
        /// </summary>
        protected void OutputMenuInternal()
        {
            Console.Clear();
            if (this.errorMessage != null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(this.errorMessage);
                Console.ResetColor();
                this.errorMessage = null;
            }

            this.OutputMenu();
            Console.WriteLine();
        }
    }
}
