﻿// <copyright file="CompanyBusinessLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PornStarMovie.Logic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using PornStarMovie.Data;
    using PornStarMovie.Logic.Logics;
    using PornStarMovie.Repository.Interfaces;
    using PornStarMovie.Repository.Repository;

    /// <summary>
    /// Logic test for CompanyBusinessLogic class
    /// </summary>
    public class CompanyBusinessLogicTest
    {
        private Mock<IRepository<Company>> mockCompany;
        private CompanyBusinessLogic companyLogic;
        private Company company;
        private List<Company> listCompany;
        private Mock<IRepository<Movies>> mockMovies;
        private List<Movies> listMovies;

        /// <summary>
        /// Test
        /// </summary>
        [Test]
        public void GetThisChingChongGetAllYourCompany()
        {
            var querry = this.companyLogic.GetAll();
            Assert.That(querry.Count(), Is.EqualTo(this.listCompany.Count()));
            this.mockCompany.Verify();
        }

        /// <summary>
        /// Test
        /// </summary>
        [Test]
        public void ChingChongShouldKnowTheIDOfTheElement()
        {
            var res = this.companyLogic.GetById(1);
            Assert.That(res.CompanyName, Is.EqualTo("Brazzer"));
        }

        /// <summary>
        /// Test
        /// </summary>
        [Test]
        public void ChingChongMustBeAbleToDeleteTheElementNotNullThrowing()
        {
            Assert.That(
                () => this.companyLogic.Delete(null), Throws.ArgumentException);
        }

        /// <summary>
        /// Test
        /// </summary>
        [Test]
        public void ChingChongMustBeAbleToDeleteTheElement()
        {
            int numberOfElement = this.listCompany.Count();
            this.companyLogic.DeleteByID(1);
            Assert.That(this.listCompany.Count(), Is.EqualTo(numberOfElement - 1));
        }

        /// <summary>
        /// Test
        /// </summary>
        [Test]
        public void GetTheDamnQuerryBitches()
        {
            var querry = this.companyLogic.GetMoviesByCompany("Brazzer");
            Assert.That(querry.Single().MovieName, Is.EqualTo("Jasmine James and BBC"));
        }

        /// <summary>
        /// Test
        /// </summary>
        [Test]
        public void GetNullExceptionFromSearchingMovieFromCompany()
        {
            Assert.That(
                () =>
            this.companyLogic.GetMoviesByCompany("Waz Company"),
            Throws.ArgumentException);
        }

        /// <summary>
        /// SetUpTheTestRepository
        /// </summary>
        [SetUp]
        public void SetUpTheTestRepository()
        {
            this.company = new Company()
            {
                CompanyID = 1,
                ComapnyBudget = 10000,
                CompanyCountry = "USA",
                CompanyName = "Brazzer",
                CompanyWebsite = "Something.com",
            };

            this.listCompany = new List<Company>()
            {
                new Company()
                {
                    CompanyID = 1,
                    ComapnyBudget = 10000,
                    CompanyCountry = "USA",
                    CompanyName = "Brazzer",
                    CompanyWebsite = "Something.com",
                },
                new Company()
                {
                    CompanyID = 2,
                    ComapnyBudget = 20000,
                    CompanyCountry = "Japan",
                    CompanyName = "BrazzerJAV",
                    CompanyWebsite = "SomethingSOME.com",
                }
            };
            this.mockCompany = new Mock<IRepository<Company>>();
            this.mockCompany.Setup(x => x.GetAll()).Returns(this.listCompany.AsQueryable());
            this.mockCompany.Setup(x => x.GetByID(It.IsAny<int>())).Returns((int id) => this.listCompany.Where(x => x.CompanyID == id).FirstOrDefault());
            this.mockCompany.Setup(x => x.Delete(It.IsAny<Company>())).Callback((Company a) => this.listCompany.Remove(a));
            this.mockCompany.Setup(x => x.Update(It.IsAny<Company>())).Callback(
                (Company a) =>
                {
                    var i = this.listCompany.FindIndex(x => x.CompanyID == a.CompanyID);
                    if (i != -1)
                    {
                        this.listCompany[i] = a;
                    }
                    else
                    {
                        this.listCompany.Add(a);
                    }
                });
            this.mockCompany.Setup(x => x.Delete(It.IsAny<int>())).Callback((int i) => this.listCompany.RemoveAt(i));
            this.listMovies = new List<Movies>()
            {
                new Movies()
                {
                    MovieCompanyID = 1,
                    MovieID = 1,
                    MovieBudget = 100000,
                    MovieDeadline = DateTime.Now,
                    MovieName = "Jasmine James and BBC",
                    Company = this.company
                }
            };
            this.mockMovies = new Mock<IRepository<Movies>>();
            this.mockMovies.Setup(x => x.GetAll()).Returns(this.listMovies.AsQueryable());
            this.companyLogic = new CompanyBusinessLogic(this.mockCompany.Object, this.mockMovies.Object);
        }
    }
}
