﻿// <copyright file="CompanyRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PornStarMovie.Repository.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PornStarMovie.Data;
    using PornStarMovie.Repository.Interfaces;

    /// <summary>
    /// CompanyRepository which implement the parent PornRepository
    /// </summary>
    public class CompanyRepository : PornRepository<Company>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CompanyRepository"/> class.
        /// Constructor for the class
        /// </summary>
        /// <param name="context">DbContext</param>
        public CompanyRepository(DbContext context)
            : base(context)
        {
        }

        /// <summary>
        /// Get the company by the id
        /// </summary>
        /// <param name="id">int type </param>
        /// <returns>the company</returns>
        public override Company GetByID(int id)
        {
            return this.GetAll().SingleOrDefault(company => company.CompanyID == id);
        }

        /// <summary>
        /// Modify the company calue
        /// </summary>
        /// <param name="companyID">companyID</param>
        /// <param name="companyName">companyName</param>
        /// <param name="companyCountry">companyCountry</param>
        /// <param name="companyWebsite">companyWebsite</param>
        public void Modify(int companyID, string companyName, string companyCountry, string companyWebsite)
        {
            Company company = this.GetByID(companyID);
            if (company == null)
            {
                throw new ArgumentException("Your company is not exist");
            }

            if (companyName != null)
            {
                company.CompanyName = companyName;
            }

            if (companyCountry != null)
            {
                company.CompanyCountry = companyCountry;
            }

            if (companyWebsite != null)
            {
                company.CompanyWebsite = companyWebsite;
            }
        }
    }
}
